import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PaymentezModule } from './paymentez/paymentez.module';

@Module({
  imports: [PaymentezModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
