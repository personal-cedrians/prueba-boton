export interface OrderInterface {
    amount?: number;
    description?: string;
    dev_reference?: string;
    discount?: number;
    vat?: number;
    installments?: number;
    installments_type?: number;
    taxable_amount?: number;
    tax_percentage?: number;
    tip?: number;
}
