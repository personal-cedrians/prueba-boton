export interface UserInterface {
    id?: string;
    email?: string;
    phone?: string;
    ip_address?: string;
    fiscal_number?: string;
}
