export interface CardInterface {
    number?: string;
    card_auth?: string;
    holder_name?: string;
    expiry_month?: number;
    expiry_year?: number;
    cvc?: string;
    type?: string;
    token?: string;
    nip?: string;
}
