import * as hash from 'hash.js';
import { Base64 } from 'js-base64';

export function authTokenGenerator(APP_KEY: string, APP_CODE: string) {
    const timestamp = Math.floor(new Date().getTime() / 1000);

    const uniqTokenString = APP_KEY + timestamp;
    const uniqTokenHash = hash.sha256().update(uniqTokenString).digest('hex');
    const authToken = Base64.encode(`${APP_CODE};${timestamp};${uniqTokenHash}`);

    return authToken;
}
