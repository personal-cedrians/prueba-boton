import {HttpService, Injectable} from '@nestjs/common';
import {authTokenGenerator} from '../funciones/auth-token-generator';
import {ENVIROMENT} from '../enviroments/enviroment';
import {CLIENT_APP_CODE, CLIENT_APP_KEY, SERVER_APP_CODE, SERVER_APP_KEY} from '../constantes/keys';

@Injectable()
export class PaymentezService {

    constructor(private readonly httpService: HttpService) {}

    ingresarTarjeta(tarjeta: any) {

        const authToken = authTokenGenerator(CLIENT_APP_KEY, CLIENT_APP_CODE);
        const segmento = '/v2/card/add';

        return this.httpService.post(ENVIROMENT.url + segmento, tarjeta, {
            headers: {
                'Auth-Token': authToken,
                'Content-Type': 'application/json',
            },
        });
    }

    comprarToken(datosCompra: any) {
        const authToken = authTokenGenerator(SERVER_APP_KEY, SERVER_APP_CODE);
        const segmento = '/v2/transaction/debit/';

        return this.httpService.post(ENVIROMENT.url + segmento, datosCompra,
            {
                headers: {
                    'Auth-Token': authToken,
                    'Content-Type': 'application/json',
                },
            },
            );

    }

    comprarTarjeta(tarjeta: any) {
        // no funciona
        const authToken = authTokenGenerator(CLIENT_APP_KEY, CLIENT_APP_CODE);
        const segmento = '/v2/transaction/debit_cc/';

        return this.httpService.post(ENVIROMENT.url + segmento, tarjeta, {
            headers: {
                'Auth-Token': authToken,
                'Content-Type': 'application/json',
            },
        });
    }

    consultarTarjetas(user: string) {
        const authToken = authTokenGenerator(SERVER_APP_KEY, SERVER_APP_CODE);
        const segmento = `/v2/card/list?uid=${user}`;
        return this.httpService.get(ENVIROMENT.url + segmento,
            {
                headers: {
                    'Auth-Token': authToken,
                    'Content-Type': 'application/json',
                },
            },
        );
    }

    eliminarTarjeta(info) {
        const authToken = authTokenGenerator(SERVER_APP_KEY, SERVER_APP_CODE);
        console.log(info);
        const segmento = '/v2/card/delete/';

        return this.httpService.post(ENVIROMENT.url + segmento, info,
            {
                headers: {
                    'Auth-Token': authToken,
                    'Content-Type': 'application/json',
                },
            },
            );
    }
}
