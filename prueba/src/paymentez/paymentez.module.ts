import {HttpModule, Module} from '@nestjs/common';
import { PaymentezController } from './paymentez.controller';
import { PaymentezService } from './paymentez.service';

@Module({
  controllers: [PaymentezController],
  providers: [PaymentezService],
  imports: [
    HttpModule,
  ],
})
export class PaymentezModule {}
