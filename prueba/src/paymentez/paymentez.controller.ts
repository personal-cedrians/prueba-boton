import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {PaymentezService} from './paymentez.service';

@Controller('paymentez')
export class PaymentezController {

  constructor(
      private readonly _paymentezService: PaymentezService,
  ) {}

  @Post('comprar')
  comprar(
    @Body() parameters,
  ) {
     this._paymentezService.comprarToken(parameters)
         .subscribe(
        (respuesta) => {
          console.log(respuesta.data);
        },
        error => {
            console.log(error.response.data);
        },
    );
  }

  @Post('add-tarjeta')
  addTarjeta(
      @Body() parameters,
  ) {
      this._paymentezService.ingresarTarjeta(parameters)
          .subscribe(
              (respuesta) => {
                  console.log(respuesta.data);
              },
              error => {
                  console.error(error.response.data);
              },
          );
  }

    @Post('comprar-tarjeta')
    comprarRegistrarTarjeta(
        @Body() parameters,
    ) {
        this._paymentezService.comprarTarjeta(parameters)
            .subscribe(
                (respuesta) => {
                    console.log(respuesta.data);
                },
                error => {
                    console.log(error.response.data);
                },
            );
    }

    @Get('tarjetas/:user')
    traerTarjetas(
        @Param('user') user,
    ) {
      this._paymentezService.consultarTarjetas(user)
          .subscribe(
              (respuesta) => {
                  console.log(respuesta.data);
              },
              error => {
                  console.log(error.response.data);
              },
          );
    }

    @Post('eliminar-tarjeta')
    eliminarTarjeta(
        @Body() info,
    ) {
      this._paymentezService.eliminarTarjeta(info)
          .subscribe(
              (respuesta) => {
                  console.log(respuesta.data);
              },
              error => {
                  console.log(error.response.data);
              },
          );
    }
}
